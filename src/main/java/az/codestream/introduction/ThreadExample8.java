package az.codestream.introduction;

public class ThreadExample8 {
    private static class StoppableRunnable implements Runnable {
        private boolean stopRequested = false;

        public synchronized void requestStop() {
            stopRequested = true;
        }

        public synchronized boolean isStopRequested() {
            return this.stopRequested;
        }

        private void sleep(long millis) {
            try {
                Thread.sleep(millis);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public void run() {
            System.out.println("StoppableRunnable Running");
            while (!isStopRequested()) {
                System.out.println(Thread.currentThread().getName() + " is running...");
                sleep(1000);
            }
            System.out.println("StoppableRunnable Stopped");
        }
    }

    public static void main(String[] args) {
        StoppableRunnable runnable = new StoppableRunnable();
        Thread t = new Thread(runnable, "The Thread 1");
        t.start(); // Thread 1
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("Stopping Thread 1");
        runnable.requestStop();
        System.out.println("Thread 1 stopped");
        /*
        StoppableRunnable Running
        The Thread 1 is running...
        The Thread 1 is running...
        The Thread 1 is running...
        The Thread 1 is running...
        The Thread 1 is running...
        Stopping Thread 1
        Thread 1 stopped
        StoppableRunnable Stopped
         */
    }
}
