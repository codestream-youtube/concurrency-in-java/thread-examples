package az.codestream.introduction;

public class ThreadExample7 {

    public static void main(String[] args) {
        Runnable runnable = () -> {
            System.out.println(Thread.currentThread().getName() + " is running after sleeping for 2 seconds");

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        };
        Thread t = new Thread(runnable, "The Thread 1");
        t.start(); // Thread 1
    }
}
