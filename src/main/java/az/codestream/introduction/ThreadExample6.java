package az.codestream.introduction;

public class ThreadExample6 {

    public static void main(String[] args) {
        // Create new runnable as lambda expression and then pass it to new thread
        Runnable runnable = () -> System.out.println(Thread.currentThread().getName() + " is running");
        Thread t = new Thread(runnable, "The Thread 1");
        Thread t2 = new Thread(runnable, "The Thread 2");

        t.start(); // Thread 1
        t2.start(); // Thread 2
    }
}
