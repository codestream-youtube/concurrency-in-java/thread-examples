package az.codestream.introduction;

public class ThreadExample1 {
    public static void main(String[] args) {
        Thread t = new Thread();
        t.start(); // New Thread starts executing and stops immediately
    }
}
