package az.codestream.introduction;

public class ThreadExample5 {

    public static void main(String[] args) {
        // Create new runnable as lambda expression and then pass it to new thread
        Runnable runnable = () -> System.out.println("New Thread starts executing and stops after console output");
        Thread t = new Thread(runnable);
        // More narrow than the above example.
        Thread t2 = new Thread(() -> System.out.println("New Thread starts executing and stops after console output"));
        t.start(); // New Thread starts executing and stops after console output
        t2.start(); // New Thread starts executing and stops after console output
    }
}
