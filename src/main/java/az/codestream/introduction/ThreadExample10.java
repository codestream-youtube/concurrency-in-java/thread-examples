package az.codestream.introduction;

public class ThreadExample10 {
    public static void main(String[] args) {
        System.out.println("Main thread is running");
        Runnable runnable = () -> {
            for (int i = 0; i < 10; i++) {
                try {
                    System.out.println(Thread.currentThread().getName() + " is running..." + i + 1);
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        Thread t = new Thread(runnable, "The Thread 1");
        t.setDaemon(true);
        t.start(); // Thread 1
        try {
            // join() method has to be called after the thread is started
            t.join(); // Try commenting this line to see the difference
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("Main thread is stopping");
    }
    /*
    // Without thread.join()
        Main thread is running
        Main thread is stopping
     */

    /*
    //  With thread.join()
        Main thread is running
        The Thread 1 is running...0
        The Thread 1 is running...1
        The Thread 1 is running...2
        The Thread 1 is running...3
        The Thread 1 is running...4
        The Thread 1 is running...5
        The Thread 1 is running...6
        The Thread 1 is running...7
        The Thread 1 is running...8
        The Thread 1 is running...9
        Main thread is stopping
     */
}
