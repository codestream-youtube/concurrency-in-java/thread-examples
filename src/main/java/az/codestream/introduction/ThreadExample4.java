package az.codestream.introduction;

public class ThreadExample4 {

    public static void main(String[] args) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("New Thread starts executing and stops after console output");
            }
        };
        Thread t = new Thread(runnable);
        t.start(); // New Thread starts executing and stops after console output
    }
}
