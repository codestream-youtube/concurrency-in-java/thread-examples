package az.codestream.introduction;

public class ThreadExample9 {
    public static void main(String[] args) {
        System.out.println("Main thread is running");
        Runnable runnable = () -> {
            while (true) {
                System.out.println(Thread.currentThread().getName() + " is running...");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        Thread t = new Thread(runnable, "The Thread 1");
        t.setDaemon(true); // Comment this line to see the difference
        t.start(); // Thread 1
        try {
            Thread.sleep(3100); // Thread 1
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("Main thread is stopping");
    }
    /*
        Main thread is running
        The Thread 1 is running...
        The Thread 1 is running...
        The Thread 1 is running...
        The Thread 1 is running...
        Main thread is stopping
     */
}
