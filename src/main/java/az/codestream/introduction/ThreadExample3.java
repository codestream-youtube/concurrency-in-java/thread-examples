package az.codestream.introduction;

public class ThreadExample3 {

    private static class MyRunnable implements Runnable {
        @Override
        public void run() {
            System.out.println("New Thread starts executing and stops after console output");
        }
    }

    public static void main(String[] args) {
//        Runnable r = new MyRunnable();
//        Thread t = new Thread(r);
        Thread t = new Thread(new MyRunnable());
        t.start(); // New Thread starts executing and stops after console output
    }
}
