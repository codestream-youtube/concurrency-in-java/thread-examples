package az.codestream.introduction;

public class ThreadExample2 {
    // It's not recommended way to create a thread.
    private static class MyThread extends Thread {
        @Override
        public void run() {
            System.out.println("New Thread starts executing and stops after console output");
        }
    }

    public static void main(String[] args) {
        MyThread t = new MyThread();
        t.start(); // New Thread starts executing and stops after console output
    }
}
